import pytest

from harper.music.notes import (
    A4,
    C8,
    BFLAT2,
    add_interval,
    ASHARP4,
    B4,
    AFLAT4,
    A5,
)

notes = [A4(), C8(), BFLAT2()]


@pytest.mark.parametrize("note", notes)
def test_notes_frequency_is_int(note):
    assert isinstance(note.frequency, float)


@pytest.mark.parametrize("note", notes)
def test_notes_sample_0_is_always_0(note):
    assert note.signal[0] == 0


@pytest.mark.parametrize("note", notes)
def test_note_cannot_be_added(note):
    try:
        note + note
    except AttributeError:
        assert True
    except TypeError:
        assert True
    else:
        assert False


@pytest.mark.parametrize("note", notes)
def test_note_frequency_equals_signal_dominant(note):
    assert int(round(note.frequency)) == int(round(note.signal.dominant))


def test_note_add_half_step_returns_sharp():
    a4 = A4()
    a4_sharp = add_interval(a4, 1)
    assert isinstance(a4_sharp, ASHARP4)


def test_note_add_whole_step_returns_next_white_note():
    a4 = A4()
    b4 = add_interval(a4, 2)
    assert isinstance(b4, B4)


def test_note_subtract_interval_returns_flat():
    a4 = A4()
    a4_flat = add_interval(a4, -1)
    assert isinstance(a4_flat, AFLAT4)


def test_note_add_12_half_steps_is_octave():
    a4 = A4()
    a5 = add_interval(a4, 12)
    assert isinstance(a5, A5)
