import random

import pytest

from harper.synthesis import generate_sine_sample

fs = [1, 2, 100, 440, 10000]

samples = range(0, 200_000)


@pytest.mark.parametrize("frequency", fs)
def test_generate_sine_sample_return_int(frequency):
    assert generate_sine_sample(random.randint(0, 100_000), frequency)


@pytest.mark.parametrize("frequency", fs)
def test_generate_sine_sample_returns_values_between_neg_16000_pos_16000(
    frequency,
):
    for x in samples:
        assert -16000 <= generate_sine_sample(x, frequency) <= 16000
