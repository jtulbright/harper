# import pytest

from harper.audio.signal import Signal
from harper.audio.sampling import resample, downsample, upsample

sig1 = Signal.from_timeseries(list(range(0, 400)))


def test_downsample_requires_ratio_under_1():
    try:
        s = downsample(sig1, 1.1)
    except ValueError:
        assert True
    else:
        assert False


def test_downsample_returns_signal():
    s = downsample(sig1, 0.9)
    assert isinstance(s, Signal)


def test_downsample_timeseries_has_approximately_ratio_less_samples():
    s = downsample(sig1, 0.9)
    assert len(s) == len(sig1) * 0.9


def test_upsample_returns_signal():
    s = upsample(sig1, 1)
    assert isinstance(s, Signal)


def test_upsample_1_returns_signal_of_2x_length():
    s = upsample(sig1, 1)
    assert len(s) == 2 * len(sig1)


def test_upsample_2_returns_signal_of_3x_length():
    s = upsample(sig1, 2)
    assert len(s) == 3 * len(sig1)


def test_upsampled_signal_has_a_lot_of_zeroes():
    s = upsample(sig1, 2)
    assert len([t for t in s if t == 0]) > 799


def test_resample__gt_1_returns_signal():
    s = resample(sig1, 2)
    assert isinstance(s, Signal)


def test_resample_lt_1_returns_signal():
    s = resample(sig1, 0.9)
    assert isinstance(s, Signal)
