import random

import numpy as np
import pytest

from harper.audio.signal import Signal

random.seed(4311)
sig1 = Signal.from_timeseries([1, 2, 3, 4, 5, 6, 7, 8, 9, -10])
sig2 = Signal.from_timeseries(
    [random.randint(0, 20000) for x in range(0, 10000)]
)
sig3 = Signal.from_timeseries([1, 300, 3, 4, 5, 6, 7, 8, 9, -10])
sig4 = Signal.from_timeseries([1, 2, 3, 4000000])
sig5 = Signal.from_timeseries([1, 2, 3, 2147483649])


from_audio = sig1
from_complex_spectrum = Signal.from_complex_spectrum(
    np.array(
        [
            35.0 + 0.00000000e00j,
            -21.18033989 + 3.63271264e00j,
            -11.18033989 - 1.21392207e01j,
            1.18033989 - 1.53884177e01j,
            11.18033989 - 1.01311066e01j,
            15.0 - 8.88178420e-16j,
            11.18033989 + 1.01311066e01j,
            1.18033989 + 1.53884177e01j,
            -11.18033989 + 1.21392207e01j,
            -21.18033989 - 3.63271264e00j,
        ]
    )
)
from_complex_timeseries = Signal.from_complex_timeseries(
    np.array(
        [
            1.0 - 4.44089210e-16j,
            2.0 + 4.44089210e-16j,
            3.0 - 6.95428534e-17j,
            4.0 + 5.36485882e-16j,
            5.0 + 7.45309152e-16j,
            6.0 + 1.02205383e-17j,
            7.0 - 3.48103688e-16j,
            8.0 - 4.07426003e-16j,
            9.0 - 3.27662611e-16j,
            -10.0 - 1.39280417e-16j,
        ]
    )
)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_size_is_num_bytes(sig):
    assert sig.size == len(sig.bytes)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_bytes_returns_byte_string(sig):
    assert isinstance(sig.bytes, bytes)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_reversed_bytes_same_type_as_bytes(sig):
    assert type(sig.bytes) == type(sig.reversed_bytes)  # noqa: E721


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_reversed_bytes_same_size_as_bytes(sig):
    assert sig.size == len(sig.reversed_bytes)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_spectrum_is_list(sig):
    assert isinstance(sig.spectrum, list)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_spectrum_is_same_size_as_timeseries(sig):
    assert len(sig.spectrum) == len(sig.timeseries)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_largest_absolute_is_correct(sig):
    assert max([abs(x) for x in sig.timeseries])


def test_signal_small_numbers_require_1_byte():
    assert sig1.bytesPerSample == 1


def test_signal_medium_numbers_require_2_bytes():
    assert sig3.bytesPerSample == 2


def test_signal_large_numbers_require_4_bytes():
    assert sig4.bytesPerSample == 4


def test_signal_very_large_numbers_require_4_bytes():
    assert sig5.bytesPerSample == 8


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_reversed_time_series_same_length_as_timeseries(sig):
    assert len(sig.timeseries) == len(sig.reversed_timeseries)


def test_signal_top_frequencies_is_int():
    assert isinstance(from_complex_timeseries.top_frequencies(1), np.ndarray)


def test_signal_large_frequencies_is_correct():
    assert from_complex_spectrum.top_frequencies(1)[0] == 9


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_read_bytes_keeps_position(sig):
    first = sig.tell
    sig.read_bytes(10)
    second = sig.tell
    assert first != second


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_seek_returns_to_position_(sig):
    sig.read_bytes(1)
    assert sig.tell != 0
    sig.seek()
    assert sig.tell == 0


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_timeseries_tell_returns_int(sig):
    assert isinstance(sig.timeseries_tell, int)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_seek_reset_timeseries_tell_position(sig):
    sig.read_bytes(10)
    assert sig.timeseries_tell != 0
    sig.seek()
    assert sig.timeseries_tell == 0


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_read_bytes_returns_position(sig):
    assert isinstance(sig.read_bytes(1), bytes)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_signal_length_equal_to_timeseries_length(sig):
    assert len(sig.timeseries) == len(sig)


def test_signal_constructors_return_eq_signals():
    assert from_audio == from_complex_timeseries == from_complex_spectrum


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_x_plot_is_iterable(sig):
    sig.x_plot.__iter__()
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_x_plot_is_iterable_of_numeric(sig):
    for x in sig.x_plot:
        try:
            x + 0
        except AttributeError:
            assert False
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_x_plot_spectrum_is_iterable(sig):
    sig.x_plot_spectrum.__iter__()
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_x_plot_spectrum_is_iterable_of_numeric(sig):
    for x in sig.x_plot_spectrum:
        try:
            x + 0
        except AttributeError:
            assert False
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_y_plot_is_iterable(sig):
    sig.y_plot.__iter__()
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_y_plot_is_iterable_of_ints(sig):
    for y in sig.y_plot:
        try:
            y + 0
        except AttributeError:
            assert False
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_y_plot_spectrum_is_iterable(sig):
    sig.y_plot_spectrum.__iter__()
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_y_plot_is_iterable_of_ints(sig):
    for y in sig.y_plot_spectrum:
        try:
            y + 0
        except AttributeError:
            assert False
    assert True


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_plot_spectrum_equal_length_range_and_domain(sig):
    assert len(sig.x_plot_spectrum) == len(sig.y_plot_spectrum)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_plot_time_equal_length_range_and_domain(sig):
    assert len(sig.x_plot) == len(sig.y_plot)


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_add_fails_on_non_signal(sig):
    try:
        sig + 2
    except ValueError:
        assert True
    else:
        assert False


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_eq_fails_on_non_signal(sig):
    try:
        sig == 2
    except ValueError:
        assert True
    else:
        assert False


@pytest.mark.parametrize("sig", [sig1, sig2, sig3, sig4, sig5])
def test_signal_len_equals_timeseries_len(sig):
    assert len(sig.timeseries) == len(sig)


def test_signal_adding_uneven_signals_doesnt_raise():
    sig1 + sig5
    assert True


def test_signal_adding_uneven_signals_generates_signal_of_longer_length():
    # sig1 has length 10
    # sig5 has length 4
    # so new_sig has length 10
    new_sig = sig1 + sig5
    assert len(new_sig) == 10


def test_signal_adding_uneven_signals_preserves_longer_where_no_overlap():
    # sig1 has length 10
    # sig5 has length 4
    # so new_sig has length 10
    new_sig = sig1 + sig5
    assert new_sig[4:] == sig1[4:]


def test_signal_adding_uneven_signals_does_not_preserver_overlap():
    # sig1 has length 10
    # sig5 has length 4
    # so new_sig has length 10
    new_sig = sig1 + sig5
    assert new_sig[0:4] != sig1[0:4]
