from harper.audio.signal import Signal
from harper.io.wavfile import to_wavfile


sig = Signal.from_timeseries([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])


def test_to_wavfile_returns_nothing():
    x = to_wavfile(sig, outfile="testout.wav")
    assert x is None


def test_to_wavfile_can_be_little_or_big_endian():
    to_wavfile(sig, outfile="test_big_endian.wav", endian="big")
    to_wavfile(sig, outfile="test_little_endian.wav", endian="little")
    assert True


def test_to_wavfile_byte_order_in_fact_changes_byte_order():
    to_wavfile(sig, outfile="test_big_endian.wav", endian="big")
    to_wavfile(sig, outfile="test_little_endian.wav", endian="little")

    # TODO: use byte operations to check the bits in the first byte
    pass
