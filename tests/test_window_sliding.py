from harper.audio.windows import sliding


def test_window_returns_list():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 1
    stride = 1
    result = sliding(seq, size=size, stride=stride)
    assert isinstance(result, list)


def test_window_returns_list_even_when_passed_tuple():
    seq = (1, 2, 3, 4, 5, 6, 7, 8)
    size = 1
    stride = 1
    result = sliding(seq, size=size, stride=stride)
    assert isinstance(result, list)


def test_window_returns_list_of_tuples_when_given_tuple():
    seq = (1, 2, 3, 4, 5, 6, 7, 8)
    size = 1
    stride = 1
    result = sliding(seq, size=size, stride=stride)
    for item in result:
        assert isinstance(item, tuple)


def test_window_returns_list_of_lists_when_given_list():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 1
    stride = 1
    result = sliding(seq, size=size, stride=stride)
    for item in result:
        assert isinstance(item, list)


def test_window_elements_same_size_as_size_parameter_1():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 1
    stride = 1
    result = sliding(seq, size=size, stride=stride)
    for item in result:
        assert len(item) == size


def test_window_elements_same_size_as_size_parameter_2():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 2
    stride = 1
    unequal = "pad"
    result = sliding(seq, size=size, stride=stride, unequal=unequal)
    for item in result:
        assert len(item) == size


def test_window_last_element_smaller_with_drop():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 3
    stride = 3
    unequal = "drop"
    result = sliding(seq, size=size, stride=stride, unequal=unequal)
    print(result)
    assert len(result[-1]) < size


def test_window_last_element_same_size_with_pad():
    seq = [1, 2, 3, 4, 5, 6, 7, 8]
    size = 3
    stride = 3
    unequal = "pad"
    result = sliding(seq, size=size, stride=stride, unequal=unequal)
    print(result)
    assert len(result[-1]) == size
