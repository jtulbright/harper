from harper.io.wavfile import WavFile, _get_endian


wav_path = "./tests/test_resources/stereo_16bit_44100hz.wav"

wv = WavFile(wav_path)


def test_wavfile_assert_true():
    assert True


def test_wavfile_filesize_returns_int():
    assert isinstance(wv.filesize, int)


def test_wavfile_filesize_reasonable():
    assert wv.filesize > 1000


def test_wavfile_size_less_than_filesize():
    assert wv.size < wv.filesize


def test_wavfile_bytes_per_sample_returns_int_less_than_10():
    assert isinstance(wv.bytesPerSample, int)
    assert wv.bytesPerSample < 10


def test_wavfile_read_bytes_returns_bytes():
    assert isinstance(wv.read_bytes(2), bytes)


def test_wavfile_get_endian_returns_string():
    assert isinstance(_get_endian(wv._bytes), str)


def test_wavheader_samplerate_returns_int():
    assert isinstance(wv._header.sampleRate, int)


def test_wavheader_framerate_less_than_equal_sample_rate():
    assert wv._header.frameRate <= wv._header.sampleRate


def test_wavheader_framerate_sample_rate_over_channels():
    assert (
        wv._header.frameRate == wv._header.sampleRate / wv._header.numChannels
    )


def test_wavdata_length_is_size_of_wavfile():
    assert wv.sizeOfData == wv.size
