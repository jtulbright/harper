import pytest

from harper.music.chords import Chord
from harper.music.notes import C0, C1, C3, E0, G0, Note

c1 = Chord([C0, E0, G0])
c1_b = Chord([E0, C0, G0])
c2 = Chord([C1, C3, C0])


def test_chord_canon_has_just_classes_and_not_instances():
    for elem in c1._canon:
        assert isinstance(elem, object)


def test_chord_notes_are_fucking_notes():
    for elem in c1.notes:
        assert isinstance(elem, Note)


def test_chord_notes_are_sorted_by_frequency():
    assert c2.notes[-1] == C3()
    assert c2.notes[0] == C0()

    assert c1._canon != c1_b._canon
    assert c1.notes == c1_b.notes


def test_chord_addition_result_in_chord():
    assert isinstance(c1 + c2, Chord)


def test_chord_addition_with_note_results_in_chord():
    assert isinstance(c1 + C0(), Chord)
