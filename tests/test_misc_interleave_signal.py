from harper.misc import interleave_signals
from harper.audio.signal import Signal


sig1 = Signal.from_timeseries([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
sig2 = Signal.from_timeseries([10, 20, 30, 40, 50, 60, 70, 80, 90, 100])
sig3 = Signal.from_timeseries([100, 200, 300, 400])

result12 = interleave_signals([sig1, sig2])
result13 = interleave_signals([sig1, sig3])
result31 = interleave_signals([sig3, sig1])
result_all = interleave_signals([sig1, sig2, sig3])


def test_interleave_signals_returns_signal():
    assert isinstance(result12, Signal)


def test_interleave_signals_works_with_equal_lengths():
    # thanks, black
    expected = [
        1,
        10,
        2,
        20,
        3,
        30,
        4,
        40,
        5,
        50,
        6,
        60,
        7,
        70,
        8,
        80,
        9,
        90,
        10,
        100,
    ]
    assert expected == result12.timeseries


def test_interleave_works_if_second_signal_shorter():
    expected = [
        1,
        100,
        2,
        200,
        3,
        300,
        4,
        400,
        5,
        0,
        6,
        0,
        7,
        0,
        8,
        0,
        9,
        0,
        10,
        0,
    ]
    assert expected == result13.timeseries


def test_interleave_works_if_first_signal_shorter():
    expected = [
        100,
        1,
        200,
        2,
        300,
        3,
        400,
        4,
        0,
        5,
        0,
        6,
        0,
        7,
        0,
        8,
        0,
        9,
        0,
        10,
    ]
    assert expected == result31.timeseries


def test_interleave_works_with_3_signals():
    expected = [
        1,
        10,
        100,
        2,
        20,
        200,
        3,
        30,
        300,
        4,
        40,
        400,
        5,
        50,
        0,
        6,
        60,
        0,
        7,
        70,
        0,
        8,
        80,
        0,
        9,
        90,
        0,
        10,
        100,
        0,
    ]

    expected == result_all.timeseries
