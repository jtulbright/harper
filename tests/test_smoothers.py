from harper.audio.smoothers import sliding_average_smooth
from harper.audio.signal import Signal


short = Signal.from_timeseries([10, 20, 90, 40, -100, 60])
smoothed_short = sliding_average_smooth(short, m=3)


def test_smoothers_sliding_average_returns_signal():
    assert isinstance(smoothed_short, Signal)


def test_smoothers_sliding_average_returns_start_and_end_are_same():
    assert smoothed_short[0] == 10
    assert smoothed_short[-1] == 60


def test_smoothers_sliding_average_changes_internal_values():
    assert smoothed_short[1] != 20
    assert smoothed_short[2] != 90
    assert smoothed_short[3] != 40
    assert smoothed_short[4] != -100


def test_smoothers_sliding_averags_averages_internal_values():
    assert smoothed_short[1] == 40
    assert smoothed_short[2] == 57
    assert smoothed_short[3] == -1
    assert smoothed_short[4] == -14
