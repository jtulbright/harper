import pytest
from harper.audio.filters import (
    highpass_filter,
    lowpass_filter,
    amplify_signal,
    normalize_signal,
)
from harper.audio.signal import Signal
from harper.music.notes import A4, A3


filters = [highpass_filter, lowpass_filter]
signal = Signal.from_timeseries([1, 2, 3, 4, 5, 99])


@pytest.mark.parametrize("filter", filters)
def test_filters_return_signal(filter):
    s = filter(signal, threshold=0)
    assert isinstance(s, Signal)


def test_highpass_filter_leaves_high_values_alone():
    s = highpass_filter(signal, threshold=3)

    assert s.spectrum[3:] == signal.spectrum[3:]


def test_lowpass_filter_leaves_low_values_alone():
    s = lowpass_filter(signal, threshold=3)

    assert s.spectrum[:3] == signal.spectrum[:3]


def test_amplift_signal_returns_signal():
    s = amplify_signal(signal, 2)
    assert isinstance(s, Signal)


def test_amplify_signal_makes_largest_sample_double():
    s = amplify_signal(signal, 2)
    largest_original = max(signal.timeseries)
    largest_new = max(s.timeseries)
    assert largest_new == largest_original * 2


def test_normalize_signal_returns_signal():
    s = normalize_signal(signal, 5)
    assert isinstance(s, Signal)


def test_normalize_signal_returns_correct_magnitude():
    s = normalize_signal(signal, 5)
    assert max(s.timeseries) == 5
