import pytest
import numpy as np
from numpy.testing import assert_array_equal

from harper.audio.extension import (
    zero_extension,
    constant_extension,
    even_extension,
)

arr1 = np.array([1, 2, 3])


def test_padding_zero_extension_returns_array():
    assert isinstance(zero_extension(arr1, 1), np.ndarray)


ARRAY_ANSWERS = ("array, padding, answer", [(arr1, 2, 7), (arr1, 5, 13)])


@pytest.mark.parametrize(*ARRAY_ANSWERS)
def test_padding_zero_extension_returns_array_of_correct_length(
    array, padding, answer
):
    output = zero_extension(array, padding)
    assert len(output) == answer


def test_padding_const_pad_returns_array():
    assert isinstance(constant_extension(arr1, 1), np.ndarray)


@pytest.mark.parametrize(*ARRAY_ANSWERS)
def test_padding_const_pad_returns_array_of_correct_length(
    array, padding, answer
):
    output = constant_extension(array, padding)
    assert len(output) == answer


def test_mirror_gets_correct_answer():
    mirrored = even_extension(arr1, 2)
    assert_array_equal(mirrored, np.array([3, 2, 1, 2, 3, 2, 1]))
