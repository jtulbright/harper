from harper.audio.signal import Signal
from harper.audio.filters import lowpass_filter, normalize_signal
from harper.music.notes import A4, A3
from harper.music.chords import Chord
from harper.media import play, plot

a3 = A3()  # 220 hz
play(a3)

new = Chord([A3, A4])
play(new)

back_to_a3 = lowpass_filter(new, 300)
play(back_to_a3)

normed = normalize_signal(back_to_a3)
play(normed)

