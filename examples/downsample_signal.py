from harper.audio.sampling import downsample, interpolate, upsample
from harper.media import play


from harper.samples import THANK_GOD


play(THANK_GOD)


downsamples = downsample(THANK_GOD.signals[0], 0.5)

play(downsamples)



upsamples = upsample(THANK_GOD.signals[0], stuff_factor=3)

play(upsamples)


interpolated = interpolate(THANK_GOD.signals[0], stuff_factor=3, frequency_threshold=300)

play(interpolated)