from harper.music.notes import A4, A3
from harper.media import play, plot

a4 = A4(waveform="saw")
# a3 = A3(waveform="square")

play(a4)
# play(a3)
# plot(a3, "aw")
plot(a4, "a4.png")
