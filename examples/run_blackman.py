from harper.audio.windows import blackman
from harper.samples import THANK_GOD
from harper.media import plot, play

s = THANK_GOD.signals[0]


chunks = blackman(s, 512)

for chunk in chunks[400:]:
    plot(chunk, "hey")
    break
    play(chunk, sample_rate=22050)
