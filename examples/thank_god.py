from harper.io.wavfile import WavFile
from harper.media import play, plot
from harper.audio.signal import Signal
from harper.music.notes import ASHARP7
from harper.audio.filters import normalize_signal

wv = WavFile("./resources/thank-god-its-friday.wav")
channel_1 = wv.signals[0]
reversed_channel_1 = Signal.from_timeseries(channel_1.reversed_timeseries)

play(channel_1, sample_rate=wv.frameRate)
play(reversed_channel_1, sample_rate=wv.frameRate)