from harper.music.notes import C4, E4, G4
from harper.media import play, plot
from harper.audio.filters import normalize_signal


c4 = C4(seconds=1)
e4 = E4(seconds=2)
g4 = G4(seconds=3)

new_sig = normalize_signal(c4.signal + e4.signal + g4.signal)

play(new_sig)
# plot(new_sig, "uneven.png")
