from harper.music.notes import *
from harper.media import play
from harper.audio.filters import normalize_signal
from harper.audio.smoothers import sliding_average_smooth

duration = 0.1

mary_had_a_little_lamb = [
    E4(duration),
    D4(duration),
    C4(duration),
    D4(duration),
    E4(duration),
    E4(duration),
    E4(duration * 2),
    D4(duration),
    D4(duration),
    D4(duration * 2),
    E4(duration),
    F4(duration),
    F4(duration * 2),
    E4(duration),
    D4(duration),
    C4(duration),
    D4(duration),
    E4(duration),
    E4(duration),
    E4(duration),
    E4(duration),
    D4(duration),
    D4(duration),
    E4(duration),
    D4(duration),
    C4(duration),
]


# for note in mary_had_a_little_lamb:
#     play(note)


signal = E4().signal
for note in mary_had_a_little_lamb:
    signal = signal.append(note.signal)

signal = normalize_signal(signal)
signal = sliding_average_smooth(signal)
play(signal)
