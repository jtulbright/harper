from harper.media import plot_spectrum, plot_samples, plot
from harper.music.notes import A3


a3 = A3()

# nb: a Note is also a plottable object, so getting
# the signal out isn't strictly necessary.
signal = a3.signal

plot_samples(signal, "a3.png", x_lim=(0, 1000))
plot_spectrum(signal, "a3_spectrum.png", x_lim=(0, 600))
plot(signal, "a3_both.png")
