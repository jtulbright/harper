from harper.music.notes import A4
from harper.media import play
from harper.io.wavfile import to_wavfile
from harper.io.wavfile import WavFile

a4 = A4()
play(a4)
to_wavfile(a4.signal, "a4.wav", endian="little")


wv = WavFile("./a4.wav")
play(wv)
