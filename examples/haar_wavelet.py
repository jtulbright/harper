from harper.audio.signal import Signal
from harper.audio.transforms import haar
from harper.media import plot, play

s = Signal.from_timeseries(
    [5, 4, 5, 7, 8, 9, 0, 9, 8, 7, 6, 5, 4, 5, 4, 3, 2, 1, 2, 3, 4, 5, 4]
)


a, d = haar(s)
s2 = Signal.from_timeseries(a)

plot(s, "pre_haar")
plot(s2, "post_haar")


play(s2)
