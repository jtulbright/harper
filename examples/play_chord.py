from harper.music.notes import *
from harper.media import play, plot
from harper.audio.filters import normalize_signal
from harper.music.chords import Chord


chord = Chord([C4, E4, G4])
chord = normalize_signal(chord)
play(chord)
plot(chord, "chord.png")
