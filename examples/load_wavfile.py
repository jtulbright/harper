from harper.io.wavfile import WavFile
from harper.media import play

path_ = "./resources/stereo_16bit_44100hz.wav"
wv = WavFile(path_)
play(wv)
