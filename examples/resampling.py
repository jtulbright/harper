from harper.audio.sampling import downsample, upsample, interpolate, resample
from harper.io.wavfile import WavFile
from harper.media import play, plot
from harper.audio.smoothers import sliding_average_smooth

tgif = WavFile("./resources/thank-god-its-friday.wav")

sig1 = tgif.signals[0]
plot(sig1, "original-sample")

ratio = 0.1
original_rate = tgif.frameRate
downsample_rate = original_rate * ratio
print(downsample_rate)


d = downsample(sig1, ratio=ratio)
plot(d, "downsampled")
play(d, sample_rate=downsample_rate)

smoothed = sliding_average_smooth(d)
play(smoothed, sample_rate=downsample_rate)
plot(d, "smooth")


upsampled = upsample(sig1, 1)
interped = interpolate(sig1, 1, 22050)

play(upsampled)
plot(upsampled, "upsampled")

play(interped)
plot(interped, "interped")
