.. harper documentation master file, created by
   sphinx-quickstart on Wed Jan 29 22:03:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to harper's documentation!
==================================

.. figure:: ./_static/harp_small.png
   :align: right

harper is a tool for manipulating audio data. 

The primary data structure in harper is the, `Signal`, a one-dimenstional array of integer values. A Signal represents samples from a particular analog signal. In audio data, this is typically pressure from a sound wave. 


.. toctree::
   :maxdepth: 1

   ./audio/audio
   ./music/music
   ./io/io
   bops
   exceptions
   media
   misc
   transforms

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
