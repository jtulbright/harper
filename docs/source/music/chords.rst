
harper.music.chords module
---------------------------

.. automodule:: harper.music.chords
   :members:
   :undoc-members:
   :show-inheritance: